# Bank2010522 :open_file_folder: (2019)
This programm is a simple Bank Accounts Handler, It gives you the possibility to create, edit, update ... accounts.
All datas are stored in Sqlite3 Database (You need to change the path of you DB on source code).

# How to use.
* Step 01 : Clone this repository.
* Step 02 : Open Bank20190511.pro with QtCreator.
* Step 03 : click the Run button (Ctrl + R) & here we go.

# Requierement :
* QtCreator.

# Project heirarchy
* DB/ : Contains the Database & Sql script to create Db manually.
            Use Bank20190617.sqlite

* icons/ : Constains some icons used on the programm.

* Compte Rendu/ : Contains two PDFs describing the programm and the implementation of the Database

* \*.h \*.ui \*.cpp :  Each Window is implemented with a .cpp class (Implicitly .h and .ui), execpt the one (DbManager.h) that handle the interaction between the programm and the Database.

# Overview 
![alt text](overview.jpg)
